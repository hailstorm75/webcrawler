namespace UtWebCrawler

module ArgumentTests =
  open Xunit
  open System.Threading

  let internal runCrawler (arguments : seq<string>) =
    WebCrawler.Console.runAsync(arguments, CancellationToken())
    |> Async.RunSynchronously

  [<Literal>]
  let internal cat = "Category"
  [<Literal>]
  let internal cat_arguments = "Arguments"

  [<Theory>]
  [<InlineData "-v" >]
  [<InlineData "-o ." >]
  [<InlineData "-p 5" >]
  [<InlineData "-d 5" >]
  [<InlineData "-d 5 -p 5" >]
  [<Trait(cat, cat_arguments)>]
  let IncompleteArguments arguments =
    // Act
    let result = runCrawler(arguments |> Seq.singleton)

    // Assert
    Assert.Equal(1, result)

  [<Theory>]
  [<InlineData "abc" >]
  [<InlineData "--abc" >]
  [<InlineData "-abc" >]
  [<InlineData "-z" >]
  [<InlineData "0" >]
  [<InlineData "-0" >]
  [<Trait(cat, cat_arguments)>]
  let InvalidArguments arguments =
    // Act
    let result = runCrawler(arguments |> Seq.singleton)

    // Assert
    Assert.Equal(1, result)

  [<Fact>]
  [<Trait(cat, cat_arguments)>]
  let DisplayVersion () =
    // Assemble
    let arguments = "--version" |> Seq.singleton

    // Act
    let result = runCrawler(arguments)
    
    // Assert
    Assert.Equal(0, result)

  [<Fact>]
  [<Trait(cat, cat_arguments)>]
  let DisplayHelp () =
    // Assemble
    let arguments = "--help" |> Seq.singleton

    // Act
    let result = runCrawler(arguments)
    
    // Assert
    Assert.Equal(0, result)
