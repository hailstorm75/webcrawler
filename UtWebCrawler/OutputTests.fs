namespace UtWebCrawler

open System

module OutputTests =
  open Xunit
  open Helpers
  open System.IO

  [<Literal>]
  let test_website = "https://crawler-test.com/"
  [<Literal>]
  let output_filename = "crawl.log"

  let defaul_output = Path.Combine(Directory.GetCurrentDirectory(), output_filename)

  [<Collection("Sequential")>]
  type TestSuite() =
    interface IDisposable with
      member this.Dispose() =
        if (File.Exists defaul_output) then
          File.Delete defaul_output

    [<Fact>]
    member __.CrawlSimple () =
      // Assemble
      let arguments = [| "-u"; test_website; "-d 0"; "-p 0" |] |> Seq.ofArray

      // Act
      runCrawler arguments |> ignore

      // Assert
      Assert.True(File.Exists defaul_output)

    [<Theory>]
    [<InlineData "./aaa/bbb/crawl.log">]
    member __.OutputInvalidPaths (path : string) =
      // Assemble
      let arguments = [| "-u"; test_website; "-d 0"; "-p 0"; "-o"; path |] |> Seq.ofArray

      // Act
      let result = runCrawler arguments

      // Assert
      Assert.NotEqual(0, result)


    [<Theory>]
    [<InlineData "./aaa">]
    member __.OutputValidPaths (path : string) =
      // Assemble
      let arguments = [| "-u"; test_website; "-d 0"; "-p 0"; "-o"; path |] |> Seq.ofArray

      // Act
      let result = runCrawler arguments
      if (File.Exists path) then
        File.Delete path

      // Assert
      Assert.Equal(0, result)
