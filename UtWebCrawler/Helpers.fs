﻿namespace UtWebCrawler

open System.Threading

module Helpers =
  [<Literal>]
  let internal cat = "Category"
  [<Literal>]
  let internal cat_arguments = "Arguments"
  [<Literal>]
  let cat_output = "Output"

  let runCrawler (arguments : seq<string>) =
    WebCrawler.Console.runAsync(arguments, CancellationToken())
    |> Async.RunSynchronously

