\documentclass[11pt]{article}

%--------------------------------------------------------------------------------
% PACKAGES
%--------------------------------------------------------------------------------

\usepackage[a4paper, left=2cm, right=2cm, bottom=15mm, headheight=14pt]{geometry}
\usepackage[table]{xcolor}
\definecolor{grey}{rgb}{0.9,0.9,0.9}

\usepackage{listings}
\usepackage{inconsolata}
\usepackage{hyperref}
\usepackage{footnote}
\usepackage[utf8]{inputenc}
\usepackage[sfdefault]{ClearSans}
\usepackage{fancyhdr}
\usepackage{tabularx,booktabs}
\usepackage{tablefootnote}
\usepackage{dingbat}
\usepackage[nottoc]{tocbibind}

%--------------------------------------------------------------------------------
% LINK STYLE
%--------------------------------------------------------------------------------

\hypersetup{
  colorlinks=true,
  linkcolor=black,
  filecolor=magenta,
  urlcolor=cyan,
  pdftitle={WebCrawler thesis},
  pdfpagemode=FullScreen
}

\urlstyle{same}

%--------------------------------------------------------------------------------
% COMMANDS
%--------------------------------------------------------------------------------

\renewcommand\tabularxcolumn[1]{m{#1}}% for vertical centering text in X column

%--------------------------------------------------------------------------------
% HEADER & FOOTER
%--------------------------------------------------------------------------------

\pagestyle{fancy}
\lhead{Web Crawler}
\cfoot{}
\rhead{Page \thepage}

%--------------------------------------------------------------------------------
% LISTING STYLE
%--------------------------------------------------------------------------------

\lstset{
  basicstyle=\ttfamily,
}

\begin{document}

%--------------------------------------------------------------------------------
% TITLE PAGE
%--------------------------------------------------------------------------------

\begin{titlepage}

  %--------------------------------------------------------------------------------
  % TITLE BOX
  %--------------------------------------------------------------------------------

  \colorbox{grey}{
    \parbox[t]{0.93\textwidth}{
      \parbox[t]{0.91\textwidth}{
        \raggedleft
        \fontsize{30pt}{80pt}\selectfont
        \vspace{0.7cm}

       \textbf{Web Crawler}\large \\[24pt]
        Thesis for 4IZ110

        \vspace{0.7cm}
      }
    }
  }

  \vfill

  %--------------------------------------------------------------------------------
  % AUTHOR & INFO
  %--------------------------------------------------------------------------------

  \parbox[t]{0.93\textwidth}{
    \raggedleft
    {\Large Denis Akopyan}\\
    {\small\it University of Economics, Prague}\\[4pt]

    \hfill\rule{\textwidth}{1pt}

    \raggedright
    \begin{tabbing}
    \hspace*{3cm}\= \kill
    Date \> 23 October, 2019\\
    Course \> 4IZ110, Wen 4:15 PM\\
    Teacher \> RNDr. Radomír Palovský, CSc.\\
    \end{tabbing}
  }

\end{titlepage}

%--------------------------------------------------------------------------------
% ABSTRACT
%--------------------------------------------------------------------------------
\begin{abstract}

This thesis aims to explain what a web crawler is, how to create a one, and document the development process as well as to fulfill part of the credit requirements from the 4IZ110 course for the summer semester 2019/2020 by accomplishing the previously mentioned goals.

\end{abstract}
\newpage

%--------------------------------------------------------------------------------
% TABLE OF CONTENTS
%--------------------------------------------------------------------------------

\tableofcontents
\newpage

\section{Introduction}

\subsection{General description}

A web crawler is a bot \footnote{Short for Internet bot. It is a program that executes automated tasks over the Internet} that browses (crawls) the web and indexes web pages it comes across.

\subsection{Use cases}

Most commonly, such bots are operated by search engines to index the contents of the web. In other cases, they are used to analyze websites to improve SEO \footnote{Search Engine Optimization}. This analysis can include, among other things, checking page:

\begin{enumerate}
  \item Reachability
  \item Address
  \item Structure (HTML \footnote{HyperText Markup Language - a language which is used to define how a web page is structured})
  \item Keywords
\end{enumerate}

Additionally, web crawlers could check web page security by locating, for example - input forms, file upload fields, administrator pages, pages with access to authenticated users only, and so on. These kinds of checks can either be used to improve security or to break it. Thus, web crawlers can also be used for malicious purposes.

\subsection{How it works}

For the web crawlers to crawl, provide them with one or more web links. If, for example, someone creates a new website, its sitemap can be added to an indexing queue of a search engine.

A sitemap is a document with a list of URLs of the given website. It allows web crawlers to explore the website more efficiently because it provides additional information about each URL:
\begin{itemize}
  \item Importance
  \item Relevancy
  \item Update frequency
\end{itemize}

Contrary to the sitemap, the robots.txt file is a list of URLs that tells the web crawlers which parts of the website to exclude from indexing.

Both files complement each other and should be present on every website to improve SEO - they are intended for use by search engines. However, if someone wishes to crawl a site entirely, they ignore the robots.txt or even better include the links from the robots.txt.

\newpage
\section{Creating a web crawler}

The developed web crawler will provide basic functionality for demonstrative purposes - crawl the web starting from a given set of addresses, collect a list of visited addresses. Additionally, the program will run from the terminal/command line - meaning that there will be no GUI  \footnote{Graphical User Interface}. Developing a feature-rich web crawler would require more time than is provided for writing this thesis.

\subsection{Analysis}

\subsubsection{Choosing a git platform} \label{choosing-git}

It would be sufficient to develop the project locally; however, this introduces the risk of losing data - no way to rollback to previous changes, and generally a lack of useful automation tools.

Git \footnote{A version control system. The complete explanation of this technology and the terminology which comes with it is beyond the scope of this thesis} is the perfect solution for such issues. Then again, git can be run locally, which provides version control solving the rollback issue, but a single machine still holds the project data.

With this in mind, using an online git service is the way to go.

\vspace{5mm}
\begin{minipage}{\textwidth}
  \renewcommand{\arraystretch}{1.5}%
  \rowcolors{5}{}{grey}

  \begin{tabularx}{\textwidth}{ l c X X }
    \toprule
    \bf Platform & \bf Proficiency & \bf Pros                                                                & \bf Cons\\
    \midrule
    GitHub       & 9/10            & Great integration in 3rd party software, popular and well-known         & None\\
    GitLab       & 9/10            & Leader in CI/CD \footnote{Continuous Integration/Continuous Deployment} & Poor integration compared to GitHub\\
    Azure DevOps & 6/10            & Integrated CI/CD and Azure services \footnote{Cloud services from Microsoft for application building, managing and deployment} & Limited to 5 users\\
    Bitbucket    & 2/10            & Unknown                                                                 & Limited to 5 users, no prior experience\\
    \bottomrule
  \end{tabularx}
  \textit{Online git platforms table}

\end{minipage}
\vspace{5mm}

{\bf GitLab} will host the project because of positive prior experience with it and no known cons, which would affect development.
At close second came {\bf GitHub} - a well-established and reputable Git system that only recently added integrated CI/CD support and free private repositories.
Using {\bf Azure DevOps} would be a fantastic opportunity to gain experience in using it. It is a quality Git system, although too advanced for the needs of this project.

\subsubsection{Choosing a programming language} \label{choosing-language}

Selecting a specific programming language for this project is subjective to personal preference.
However, it would be wise to choose a language that doesn't slow down the development and provides useful features for this type of project.

\vspace{5mm}
\begin{minipage}{\textwidth}
  \renewcommand{\arraystretch}{1.5}%
  \rowcolors{5}{}{grey}

  \begin{tabularx}{\textwidth}{ l c X X }
    \toprule
    \bf Language & \bf Proficiency & \bf Pros                                                                                                                         & \bf Cons\\
    \midrule
    C            & 5/10            & Fast and efficient if used correctly                                                                                             & Painful memory management and multi-threaded programming, no OOP \footnote{Object Oriented Programming}\\
    C++          & 6/10            & Introduces OOP to C along with other higher-level programming abstractions                                                       & Painful memory management and debugging\\
    C\#          & 9/10            & Powerful and mature OOP programming language running on the .NET \footnote{A software framework developed by Microsoft} platform & None\\
    F\#          & 2/10            & Combination of FP \footnote{Functional programming} and OOP on the .NET platform                                                 & No prior experience using it\\
    Python       & 3/10            & Easy-to-use syntax                                                                                                               & Slow, little prior experience\\
    \bottomrule
  \end{tabularx}
  \textit{Programming languages table}
\end{minipage}
\vspace{5mm}

The project will be developed using {\bf F\#} because it runs on .NET supports tail-recursion \footnote{A process in which a function calls itself as the last operation. The benefits of tail-recursion to recursion are beyond the scope of this thesis} and is an opportunity to learn a new language.

\subsubsection{Program structure}

A typical .NET program consists of a {\bf Solution} containing projects.
The solution is just a configuration file that contains information about which projects are part of it, how to compile \footnote{The act of converting code to machine-readable instructions - accomplished by a compiler} those projects and where to compile them (output directories).

Projects can either be applications or libraries. An application is the starting point of the program - it is what the user or automated script runs. A library is referenced by one or more applications to extend their functionality.

The web crawler solution will consist of the following parts:

\begin{enumerate}
  \item {\bf Console} (Application) - terminal-based user interface. {\textit For more see \ref{developing-console}}
  \item {\bf Operations} (Library) - contains the web crawler logic. {\textit For more see \ref{developing-library}}
\end{enumerate}

The reason for this separation is to make the software scalable, which would allow extending the project with, for example, a GUI.

\newpage
\subsection{Development}

As decided in \ref{choosing-git} and \ref{choosing-language} the web crawler will be hosted on {\bf GitLab} (\href{https://gitlab.com/hailstorm75/webcrawler}{repository}) and will be written in {\bf F\#} using Visual Studio 2019 \footnote{IDE developed by Microsoft} and Visual Studio Code \footnote{source-code editor developed by Microsoft} IDEs \footnote{Integrated Development Environment}.
The solution projects will run on .NET Core 3.0 which is the latest framework\footnote{A software framework is a structure which serves as a support for the development of other projects using design patterns, API libraries and so on.} developed by Microsoft.

\subsubsection{Preparing the repository}

Because the project will be managed and developed by a single person, some preparation steps can be omitted to save time. Thus only the following steps are required:
\begin{enumerate}
  \item Create a {\textit README.md} \footnote{A file which contains general information about the repository}
  \item Create a {\textit .gitignore} \footnote{A git file which defines what files and folders are not to be uploaded to the repository}
  \item Create an empty Visual Studio solution
  \item Create a {\textit .gitlab-ci.yml \footnote{A GitLab-specific file which is used to define CI/CD pipelines}} and define a pipeline with:
  \begin{enumerate}
    \item Build job - compiles the code. Used to test whether the program is compilable
    \item Test job - executes unit tests (see \ref{automated-tests})
    \item Document job - generates the PDF of this thesis from \LaTeX
    \item Release job - compiles the code with optimizations and generates the PDF
  \end{enumerate}
  \item Create a folder which will hold this thesis
\end{enumerate}

The skipped steps are all related to managing a larger group of people by writing a project description, contribution guidelines, issue templates, and so on.

\subsubsection{Developing the operations library} \label{developing-library}

\paragraph{Description}  ~\\

The purpose of this project is to provide web crawling functionality to other projects which will reference it.

\paragraph{Development}  ~\\

The library will expose only one function to other projects which reference it. That function will receive a wrapped collection of arguments that define how the crawler should crawl the web and where to start, and return a sequence of visited URLs.

Accomplishing this requires the following steps:
\begin{enumerate}
  \item \label{itm:first} Iterate through the received list of URLs
  \item For every single URL do the following:
  \begin{enumerate}
    \item Return the received URL
    \item Download the URL web page
    \item Extract links from the page
    \item Filter the links
    \item Call self (recursion) and passing the new list of links (goes to step \ref{itm:first})
  \end{enumerate}
\end{enumerate}

Downloading web pages may take a lot of time, thus slowing down the whole process. Utilizing CPU cores allows processing multiple links in parallel with the help of asynchronous programming.

\subsubsection{Developing the console application} \label{developing-console}

\paragraph{Description}  ~\\

The purpose of this project is to receive input from the user and forward it to the operations library (\ref{developing-library}).

\paragraph{Development}  ~\\

The user provides the input in the form of a list of arguments that must be from a defined list (table \ref{table:webcrawler-arguments}) and must follow the correct format.
Thus the console application must be able to receive the user input, process it, and validate it.

The most important inputs are the list of starting URLs and the output path.

Receiving the input is as simple as reading the arguments passed to the application's main function\footnote{Every program has a main function. It is the first function called when running the program. Through that function the rest of the program can receive the input arguments provided by the user. A broader explanation of functions is beyond the scope of this thesis}.

The open-source NuGet \footnote{Open-source package manager from Microsoft} library \href{https://www.nuget.org/packages/CommandLineParser.FSharp/}{CommandLineParser.FSharp} solves the processing of the input. Based on the defined set of input arguments and their properties (required, input data type, etc.), it validates the input when processing it.

However, this processing is not sufficient since it validates the arguments and not the data which came with them. The argument which requires additional validation is the output path. The directories in the path must exist, and there must not exist a file with the filename in the path.

After the validation, the operations library receives the input. As described in \ref{developing-library} the produced output is a collection of crawled links. The last step is to export the output to a file defined in the output path argument. During the export process, the console can optionally log what link is being exported if the verbose argument is provided.

\subsubsection{Automated tests} \label{automated-tests}

This step is not mandatory for such a small project, but it is always useful to let the machine do the testing and the human do the programming.

Adding a library to the solution accomplishes this. It will contain unit tests. Unit tests are just like any other program; however, they run code from a given project in this case, the Console application, and compare the generated output with the expected one. If they match, the test has passed, otherwise failed.

Unit tests are most commonly integrated into the CI/CD pipeline, allowing developers to verify whether their changes have not broken existing functionality.

\newpage
\subsection{Usage}

The project was developed with a console interface only; thus, it is possible to execute it from a terminal/command line. To do so, navigate to the folder containing the web crawler binaries and run the {\bf WebCrawler.dll} file.

Although the file has the typical Window extension {\textit .dll}, it can run on both Windows and Linux distributions thanks to .NET Core. Running on a Mac was not tested.

\vspace{5mm}
\begin{minipage}{\textwidth}
  \renewcommand{\arraystretch}{1.5}%
  \rowcolors{5}{}{grey}

  \begin{tabularx}{\textwidth}{ l l l c X }
    \toprule
    Short name \footnote{Short names are to be preceded by a single dash: \ttfamily{-}} & Long name \footnote{Long names are to be preceded by a double dash: \ttfamily{--}} & Default & Required   & Description\\
    \midrule
    u          & urls      &         & \checkmark & List of comma-separated starting URLs.\newline {\bf Example:}\newline https://web.com,http://otherweb.com\\
    b          & blacklist &         &            & List of comma-separated hosts which are to be excluded from the crawl.\newline {\bf Example:}\newline bad.website.com,www.google.com\\
    o          & output    & ./crawl.log &        & Output file path\\
    d          & depth     & 10      &            & Maximum depth to crawl; depth refers to the shortest distance of a website to one of the starting sites\\
    p          & pages     & 20      &            & Maximum number of pages to discover in each link\\
    v          & verbose   &         &            & Enable verbose output\\
               & help      &         &            & Displays list of accepted arguments and their descriptions\\
               & version   &         &            & Displays program version\\
    \bottomrule
  \end{tabularx}
  \textit{WebCrawler arguments}
  \label{table:webcrawler-arguments}
\end{minipage}
\vspace{5mm}

\textbf {Linux terminal format:}
\begin{lstlisting}
  ./WebCrawler.dll -u https://ex.com --verbose -d 2 -b "web.com,www.web.com"
\end{lstlisting}

\textbf {Windows command-line format:}
\begin{lstlisting}
  dotnet WebCrawler.dll -u https://ex.com --verbose -d 2 -b "web.com,www.web.com"
\end{lstlisting}

For more detailed information on running the application please refer to the ReadMe.pdf included with this project or visit the \href{https://gitlab.com/hailstorm75/webcrawler}{repository} page to view the instructions there.

\newpage
\section{Summary}

\subsection{Experience gained}

Thanks to the development of the project and writing of this thesis, I have gained experience in the following areas:
\begin{itemize}
  \item F\# - Learning how to utilize functional programming was a great addition to the current skillset
  \item {\LaTeX} - Writing this thesis in {\LaTeX} has proven to be beneficial as the document source can be stored on git and is highly customizable
  \item URL formats - Not as important as the other two, but was a profitable learning experience
\end{itemize}

\subsection{Observations}

During the initial development, the was a consideration to exclude unit tests. Thankfully, deciding to add them proved to be very useful as the tests had uncovered multiple bugs. The test suite is not complete, and there is room for improvement; however, the main observation is that including unit tests in any project is generally a good idea.

Even if a project lacks automated testing, manual testing is mandatory. After completing the general logic, the program was tested on user-picked websites to ensure that everything was in working order. But after discovering a web crawler specific test website and using it in manual testing, multiple bugs have been found.

\subsection{Conclusion}

In conclusion, testing is an essential part of development; moreover, implementing automated tests must be done as early as possible in the development process. Overall this project has proven to be beneficial and fun to develop.

\newpage
\begin{thebibliography}{9}
  \bibitem{latexForBeginners} 
  Jiří Rybička
  \textit{\LaTeX\ pro začátečníky}. 
  KONVOJ s.r.o., Brno,
  3. vydání,
  2003,
  ISBN 80-7302-049-1.
   
  \bibitem{programmingfsharp3} 
  Chris Smith. 
  \textit{Programming F\# 3.0}.
  O'Reilly Media Inc., 1005 Gravenstein Highway North, Sebastopol,
  3rd edition,
  2013,
  ISBN 978-1-449-32029-4.
\end{thebibliography}

\end{document}
