﻿namespace WebCrawler

open System
open System.IO
open System.Threading
open System.Collections.Generic
open FSharp.Data
open FSharp.Control

module Operations =
  /// <summary>
  /// Input data model
  /// </summary>
  type InputData(urls, blacklist : seq<string>, depthLimit, pagesLimit, output) =
    let normalizePath (path : string) =
      if (path.StartsWith(".") && (path.[1] = '/' || path.[1] = '\\')) then
        Path.Combine(Directory.GetCurrentDirectory(), path.Substring(2))
      else
        path

    let m_urls       : seq<string> = urls
    let m_blacklist  : Set<string> = Set.ofSeq blacklist
    let m_depthLimit : int         = depthLimit
    let m_pagesLimit : int         = pagesLimit
    let m_output     : string      = normalizePath output

    /// <summary>
    /// List of urls to process
    /// </summary>
    member __.Urls       with get() = m_urls
    /// <summary>
    /// List of hosts not to process
    /// </summary>
    member __.Blacklist  with get() = m_blacklist
    /// <summary>
    /// Maximum depth to crawl
    /// </summary>
    member __.DepthLimit with get() = m_depthLimit
    /// <summary>
    /// Maximum number of pages to discover in each site
    /// </summary>
    member __.PagesLimit with get() = m_pagesLimit
    /// <summary>
    /// Output path for the crawl result
    /// </summary>
    member __.Output     with get() = m_output

  let private downloadDocument url = async {
    try
      let! doc = HtmlDocument.AsyncLoad(url)
      return Some doc
    with
    | :? UriFormatException
    | _ -> return None
  }

  let private normalize (link : string) =
    let slash = '/'
    let http = [|'h';'t';'t';'p';':';slash;slash|]
    let https = [|'h';'t';'t';'p';'s';':';slash;slash|]

    // Lowercase everything
    let mutable normalized = link.ToLowerInvariant()
                                 .ToCharArray()

    // Add slash to link ending
    let ending = normalized.[normalized.Length - 1]
    if   (ending = '?')   then normalized.[normalized.Length - 1] <- slash
    elif (ending = slash) then ()
    else
      let index = Array.LastIndexOf(normalized, slash)
      if (not (index <> -1
      && Array.contains '?' normalized.[index..])) then
        normalized <- Array.append normalized [|slash|]

    // Add https if missing
    if (normalized.Length < 6
    || (normalized.[0..6] <> http
     && normalized.[0..7] <> https)) then
      normalized <- Array.append https normalized

    // Convert back to string
    String normalized

  let private appendSource (url : string, source : string) =
    if url.StartsWith("/") then
      let abs = Uri(source)
      let path = source.Substring(0, abs.AbsoluteUri.Length - abs.PathAndQuery.Length) + url

      path
    else
      url

  let private extractLinks (doc : HtmlDocument, source : string) =
    let extractLink (hnode : HtmlNode) =
      hnode.TryGetAttribute("href")
      |> Option.map (fun x -> x.Value())

    let linkFilter link =
      not ((String.IsNullOrEmpty link) || link.StartsWith('#') || link = "/")

    try
      doc.Descendants["a"]                          // Find all HTML link tags
      |> Seq.choose extractLink                     // Extract the HREF from the tags
      |> Seq.filter linkFilter                      // Filter unwanted links
      |> Seq.distinct                               // Remove duplicated
      |> Seq.map (fun x -> appendSource(x, source)) // Append absolute path to relative urls
    with _ -> Seq.empty

  /// <summary>
  /// Crwawls through a given set of urls
  /// </summary>
  /// <param name="urls">Urls to crawl</param>
  /// <param name="ct">Cancellation token</param>
  let crawlUrls (input : InputData, ct : CancellationToken) = asyncSeq {
    let visited = new HashSet<_>()

    let crawlUrl url =
      let domains = new HashSet<_>()

      let getHost url =
        let uri = Uri(url)
        uri.Host

      let rec loop (url, depth) = asyncSeq {
        ct.ThrowIfCancellationRequested |> ignore

        try
          if (visited.Add url) then                             // Execute condition body if new link
            let! doc = downloadDocument url
            match doc with
            | Some doc ->
              yield url                                         // Returns newly added url

              if (depth < input.DepthLimit) then                // Execute condition body if not exceeding set limit
                let extraction = extractLinks(doc, url)
                if not (Seq.isEmpty extraction) then
                  let links = if input.PagesLimit = 0 then      // If no limit is set then get:
                                extraction                      // All links
                              else                              // Otherwise get:
                                extraction
                                |> Seq.take input.PagesLimit    // Up to the set number of links

                  for link in links do                          // Processing extracted links
                    let host = getHost link
                    if not (input.Blacklist.Contains host) then
                      let newDepth = if (domains.Add host) then // If the domain is unique
                                       depth + 1                // The crawler is deeper into the web
                                     else                       // Otherwise
                                       depth                    // It is still on the same website
                      let result = loop (link, newDepth)        // Recurse over new links
                      yield! result
            | _ -> ()
        with _ -> ()
      }

      loop (url, 0)                                             // Start crawler traversal of the web

    try
      for url in input.Urls do                                  // Process input list of urls
        for urlSet in crawlUrl(normalize url) do                // Crawl a normalized url
          yield urlSet                                          // Yield produced result
    with
    | _ -> return AsyncSeq.empty
  }
