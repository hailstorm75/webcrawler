﻿namespace WebCrawler

open CommandLine;

/// <summary>
/// Console arguments model
/// </summary>
type Options = {
  [<Option('d', "depth", Default = 10, Required = false, HelpText = "Maximum depth to crawl; depth refers to the shortest distance of a website to on of the starting sites")>]
  depthLimit : int;
  [<Option('p', "pages", Default = 20, Required = false, HelpText = "Maximum number of pages to discover in each link")>]
  pagesLimit : int;
  [<Option('o', "output", Default = "./crawl.log", Required = false, HelpText = "Output file path")>]
  output : string;
  [<Option('v', "verbose", Default = false, Required = false, HelpText = "Enable verbose output")>]
  verbose : bool;
  [<Option('u', "urls", Separator=',', Required = true, HelpText = "List of comma-separated starting URLs. Example: https://website.com,http://otherwebsite.com")>]
  startUrls : seq<string>;
  [<Option('b', "blacklist", Separator=',', Required = false, HelpText = "List of comma-separated hosts which are to be excluded from the crawl. Example: bad.website.com,www.google.com,google.com")>]
  blacklist : seq<string>;
}
