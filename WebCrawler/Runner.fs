﻿namespace WebCrawler

open CommandLine
open WebCrawler
open System
open System.IO
open System.Collections.Generic
open System.Threading
open FSharp.Control
open System.Runtime.CompilerServices
open System.Security.Permissions
open System.Security

[<assembly: InternalsVisibleTo("UtWebCrawler")>]
do()

module internal Console =

  type private 'a Validation =
  | Error of string
  | Valid of 'a

  let private useHelp =
    printfn "Use --help to view application documentation"

  let private processErrors (errors : IEnumerable<Error>) =
    let processErrorType error =
      match error with
      | ErrorType.UnknownOptionError         -> printfn "Unknown option"
      | ErrorType.MissingValueOptionError    -> printfn "Missing option value"
      | ErrorType.MissingRequiredOptionError -> printfn "Missing required option"
      | ErrorType.RepeatedOptionError        -> printfn "Option repeated"
      | ErrorType.MutuallyExclusiveSetError  -> printfn "Mutually exclusive options provided"
      | _                                    -> printfn "Error parsing input"

      useHelp

    let groupError (error : Error) =
      match error.Tag with
      | ErrorType.HelpRequestedError
      | ErrorType.HelpVerbRequestedError
      | ErrorType.VersionRequestedError
        -> true
      | _ -> false

    let partitioned = errors
                      |> Seq.groupBy groupError
                      |> Seq.toArray
    if (partitioned.[0] |> fst) then
      0
    else
      partitioned.[0]
      |> snd
      |> Seq.iter (fun x -> x.Tag |> processErrorType)
      1

  let private validateDirectoryWrite (directory : string) = 
    let permissionSet = PermissionSet(PermissionState.None)

    let writePermission = FileIOPermission(FileIOPermissionAccess.Write, if directory = "." then Directory.GetCurrentDirectory() else directory)
    permissionSet.AddPermission(writePermission) |> ignore

    permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet)

  let private validateOptions (options : Options) =
    if (Seq.isEmpty options.startUrls) then
      Error("Url list is empty")
    elif (not (String.IsNullOrEmpty options.output)) then
      let directory = Path.GetDirectoryName options.output
      if ((directory = "." || Directory.Exists directory)) then
        if (File.Exists options.output) then
          Error("A file already exists with the same name as defined in the provided output path")
        else
          Valid(options)
      else
        Error("Output directory path does not exist")
    else
      Error("Output path invalid")

  let private executeOptionsAsync (options : Options Validation, ct : CancellationToken) = async {
    match options with
    | Error(e)
      -> printfn "%s" e
         useHelp
         return 1
    | Valid(o)
      -> let input = Operations.InputData(o.startUrls, o.blacklist, o.depthLimit, o.pagesLimit, o.output)
         let data = Operations.crawlUrls(input, ct)
         let output = Exporter.OutputData(o.output, data, o.verbose)
         do! Exporter.exportCrawlDataAsync(output, ct)
         return 0
  }

  let private processOptionsAsync (options : ParserResult<Options>, ct : CancellationToken) = async {
    match options.MapResult(Some, (fun _ -> None)) with
    | None    -> return 1
    | Some(x) -> return! executeOptionsAsync (validateOptions x, ct) //validateOptions x |> executeOptionsAsync
  }

  let private evaluateOptions (options : obj, ct : CancellationToken) = async {
    match options with
    | :? NotParsed<Options>    as b -> return processErrors b.Errors
    | :? ParserResult<Options> as a -> return! processOptionsAsync (a, ct)
    | _ -> return 2
  }

  let private parseArguments arguments =
    Parser.Default.ParseArguments<Options>(arguments)

  /// <summary>
  /// Runs the program based on the provided arguments
  /// </summary>
  /// <param name="arguments">An enumeration of arguments to process</param>
  /// <param name="cancellationToken"></param>
  /// <returns>Returns 0 if no errors occurred</returns>
  let runAsync (arguments : IEnumerable<string>, cancellationToken : CancellationToken) = async {
    return! evaluateOptions ((parseArguments arguments), cancellationToken)
  }
