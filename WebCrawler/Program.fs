﻿open System
open System.Threading

[<EntryPoint>]
let main arguments =
  use cancellation = new CancellationTokenSource()

  let cancel() =
    if not cancellation.IsCancellationRequested then
      cancellation.Cancel()

  Console.CancelKeyPress |> Event.add (fun _ -> cancel())

  let program = WebCrawler.Console.runAsync(arguments, cancellation.Token)
  try
    program |> Async.RunSynchronously
  with
  | :? OperationCanceledException
    -> printfn "Cancelled"
       2
  | ex ->
    let x = match ex with
            | :? AggregateException as ae
              -> if ae.InnerExceptions.Count = 1 then ae.InnerException
                 else ex
            | _ -> ex
    printfn "%A" ex
    1
