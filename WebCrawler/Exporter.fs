namespace WebCrawler

open FSharp.Control
open System.IO
open System.Threading

module Exporter =
  /// <summary>
  /// Output data model
  /// </summary>
  type OutputData(output, data, verbose) =
    let m_output  : string           = output
    let m_data    : AsyncSeq<string> = data
    let m_verbose : bool             = verbose

    /// <summary>
    /// Output path
    /// </summary>
    member __.Output  with get() = m_output
    /// <summary>
    /// Output data
    /// </summary>
    member __.Data    with get() = m_data
    /// <summary>
    /// Toggles whether the output process is to be verbose
    /// </summary>
    member __.Verbose with get() = m_verbose


  /// <summary>
  /// Exports crawl data to a file
  /// </summary>
  /// <remarks>The provided output argument must have existing directories in the path and a filename which does not exist</remarks>
  /// <param name="output">Path to output</param>
  /// <param name="data">Data to export</param>
  /// <param name="ct">Cancellation token</param>
  let exportCrawlDataAsync (data : OutputData, ct : CancellationToken) = async {
    use stream = new FileStream(data.Output, FileMode.Create, FileAccess.Write, FileShare.None, 4096, true)
    use writer = new StreamWriter(stream)

    let output (link : string) = async {
      ct.ThrowIfCancellationRequested() |> ignore

      if data.Verbose then
        printfn "%A" link

      writer.WriteLineAsync link
      |> Async.AwaitTask
      |> ignore
    }

    data.Data
    |> AsyncSeq.iterAsync output
    |> Async.RunSynchronously
    |> ignore
  }
