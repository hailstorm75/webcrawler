# Web Crawler

A basic web crawler created for demonstrative purposes for a thesis.

## Getting Started

You can get the binaries either from the latest CI/CD release job or by building them yourself locally.

### Compiled binaries

 - Navigate to https://gitlab.com/hailstorm75/webcrawler/pipelines
 - Find the top-most entry with a **passed** status
 - Click the entry download button
 - Extract the contents of the archive

### Building locally

#### Prerequisites

 - .NET Core 3.0 SDK or higher

#### Steps

 1. Clone this repository from https://gitlab.com/hailstorm75/webcrawler.git
 2. Navigate to the repository folder
 3. Run `dotnet /p:Configuration=Release /p:Platform="Any CPU" WebCrawler.sln`
 4. The program will be compiled to the **bin/Release** folder

## Running the application

### Prerequisites

 - .NET Core 3.0 Runtime or higher

### Console arguments

| Short name | Long name | Default   | Required | Description                               |
|------------|-----------|-----------|--------- |-------------------------------------------|
| u          | urls      |             | Yes    | List of comma-separated starting URLs                                                                         |
| b          | blacklist |             | No     | List of comma-separated hosts which<br>are to be excluded from the crawl                                      |
| o          | output    | ./crawl.log | No     | Output file path                                                                                              |
| d          | depth     | 10          | No     | Maximum depth to crawl; depth refers<br>to the shortest distance of a website to<br>one of the starting sites |
| p          | pages     | 20          | No     | Maximum number of pages to discover<br>in each link                                                           |
| v          | verbose   |             | No     | Enable verbose output                                                                                         |
|            | help      |             | No     | Displays list of accepted arguments and<br>their descriptions                                                 |
|            | version   |             | No     | Displays program version                                                                                      |

### Running the application

Since the application was developed using .NET Core it can run on Windows and Linux.

#### Running on Windows

 1. Make sure you have installed both **.NET Core Runtime** and the **.NET Core Desktop Runtime** from [here](https://dotnet.microsoft.com/download/dotnet-core/current/runtime)
 2. Open the command prompt (Ctrl+R, type in cmd and press Enter) 
 3. Navigate to the folder containing the binaries
 4. Run the WebCrawler application
    - Example: `dotnet WebCrawler.dll -u https://ex.com --verbose -d 2 -b "web.com,www.web.com"`

#### Running on Linux

 1. Make sure you have installed **.NET Core** from [here](https://dotnet.microsoft.com/download/dotnet-core/current/runtime)
 2. Open the terminal
 3. Navigate to the folder containing the binaries
 4. Run the WebCrawler application
    - Example: `./WebCrawler.dll -u https://ex.com --verbose -d 2 -b "web.com,www.web.com"`

## Running the tests

This section assumes you have compiled the binaries yourself. If not, please follow the steps in [Building locally](###building-locally).

Tests are located in folders starting with Ut. Navigate to such folders and run `dotnet test`

### Tests

 - UtWebCrawler - these tests validate the WebCrawler console application user input handling

## Built With

- [.NET Core 3.0](https://dotnet.microsoft.com/) - The developer platform used
- [CommandLineParser.FSharp](https://github.com/commandlineparser/commandline) - Parser used by the console application
- [Xunit](https://xunit.net/) - The unit testing framework
- [JUnitTestLogger](https://github.com/syncromatics/JUnitTestLogger) - Converter of the Xunit output to Junit output
- [pdflatex](https://pdftex.org/) - Latex compiler

## Authors

* **Denis Akopyan**
